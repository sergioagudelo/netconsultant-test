import {cleanConsole, createAll} from './data';

const companies = createAll();

cleanConsole(7, companies);

const getNameCompany = (idCompany) => {
  return companies[idCompany].name;
};
console.log('---- EXAMPLE 7 part 1 --- ', getNameCompany(0));

const deleteCompany = (idCompany) => {
  return companies.splice(idCompany, 1);
};
console.log('---- EXAMPLE 7 part 2 --- ', deleteCompany(1));

const patchCompany = (idCompany) => {
  companies[idCompany].id = companies.length + 1;
  companies[idCompany].isOpen = !companies[idCompany + 1].isOpen;
  companies[idCompany].name = companies[idCompany + 1].name + ' - Copy';
  // usersLength modified since the description says that only user attr won´t be modified
  companies[idCompany].usersLength = companies[idCompany + 1].usersLength + 1;
  return companies[idCompany];
};
console.log('---- EXAMPLE 7 part 3 --- ', patchCompany(2));

const addUserToCompany = (idCompany, user) => {
  user.id = companies[idCompany].usersLength;
  console.log(user);
  companies[idCompany].users.push(user);
  companies[idCompany].usersLength = user.id;
  return companies[idCompany];
};
const user = {
  firstName: 'Juan',
  lastName: 'Delgado',
  age: 35,
  car: true,
};
console.log('---- EXAMPLE 7 part 4 --- ', addUserToCompany(3, user));

const putCompany = (idCompany) => {
  companies[idCompany].id = companies.length + 2;
  companies[idCompany].isOpen = !companies[idCompany + 1].isOpen;
  companies[idCompany].name = companies[idCompany + 1].name + ' - Copy';
  // usersLength modified since the description says that only user attr won´t be modified
  companies[idCompany].usersLength = companies[idCompany + 1].usersLength + 1;
  return companies[idCompany];
};
console.log('---- EXAMPLE 7 part 5 --- ', putCompany(4));

const deleteUser = (idCompany, idUser) => {
  companies[idCompany].usersLength -= 1;
  return companies[idCompany].users.splice(idUser, 1);
};
console.log('---- EXAMPLE 7 part 6 --- ', deleteUser(5, 0));

const patchUser = (idCompany, idUser) => {
  companies[idCompany].users[idUser].age = 77;
  companies[idCompany].users[idUser].car = true;
  companies[idCompany].users[idUser].firstName = 'New';
  companies[idCompany].users[idUser].id = companies[idCompany].usersLength;
  companies[idCompany].users[idUser].lastName = 'User';
  return companies[idCompany];
};
console.log('---- EXAMPLE 7 part 7 --- ', patchUser(6, 0));

const putUser = (idCompany, idUser) => {
  companies[idCompany].users[idUser].age = 77;
  companies[idCompany].users[idUser].car = true;
  companies[idCompany].users[idUser].firstName = 'New';
  companies[idCompany].users[idUser].id = companies[idCompany].usersLength + 1;
  companies[idCompany].users[idUser].lastName = 'User';
  return companies[idCompany];
};
console.log('---- EXAMPLE 7 part 8 --- ', putUser(6, 1));

const tranferUser = (oldCompany, newCompany, idUser) => {
  const user = companies[oldCompany].users[idUser];
  user.id = companies[newCompany].usersLength + 2;
  companies[oldCompany].users.splice(idUser, 1);
  companies[oldCompany].usersLength = companies[oldCompany].usersLength - 1;
  companies[newCompany].users.push(user);
  companies[newCompany].usersLength += 1;
  return companies;
};
console.log('---- EXAMPLE 7 part 9 --- ', tranferUser(5, 6, 0));

// Parte 8: Crear una función tomando como parámetro un "id" de "company" y un
// "id" de "user" que permite hacer un PUT (como con una llamada HTTP) en este
// "user".

// Parte 9: Crear una función tomando como parámetro dos "id" de "company" y
// un "id" de "user". La función debe permitir que el user sea transferido de la
// primera "company" a la segunda "company". El atributo "usersLength" de cada
// "company" debe actualizarse.

// -----------------------------------------------------------------------------
// INSTRUCTIONS IN ENGLISH

// Part 1: Create a function taking as parameter an "id" of "company" and
// returning the name of this "company".

// Part 2: Create a function taking as parameter an "id" of "company" and
// removing the "company" from the list.

// Part 3: Create a function taking as a parameter an "id" of "company" and
// allowing to make a PATCH (as with an HTTP call) on all
// attributes of this "company" except on the "users" attribute.

// Part 4: Create a function taking as parameter an "id" of "company" and a
// new "user" whose name is "Delgado", the first name "Juan", aged 35 and
// a car. The new "user" must be added to the "users" list of this
// "company" and have an automatically generated "id". The function must also modify
// the "usersLength" attribute of "company".

// Part 5: Create a function taking as parameter an "id" of "company" and
// allowing to make a PUT (as with an HTTP call) on this "company" except
// on the "users" attribute.

// Part 6: Create a function taking as a parameter an "id" of "company" and a
// "id" of "user". The function must remove this "user" from the list of "users"
// from "company" and change the attribute "usersLength" from "company".

// Part 7: Create a function taking as a parameter an "id" of "company" and a
// "id" of "user" allowing to make a PATCH (as with an HTTP call) on this
// "user".

// Part 8: Create a function taking as a parameter an "id" of "company" and a
// "id" of "user" allowing to make a PUT (as with an HTTP call) on this
// "user".

// Part 9: Create a function taking as parameter two "id" of "company" and
// an "id" of "user". The function must allow the user to be transferred as a parameter
// from the 1st "company" to the 2nd "company". The "usersLength" attribute of each
// "company" must be updated.

// -----------------------------------------------------------------------------
// INSTRUCTIONS EN FRANÇAIS

// Partie 1 : Créer une fonction prenant en paramètre un "id" de "company" et
// retournant le nom de cette "company".

// Partie 2 : Créer une fonction prenant en paramètre un "id" de "company" et
// supprimant la "company" de la liste.

// Partie 3 : Créer une fonction prenant en paramètre un "id" de "company" et
// permettant de faire un PATCH (comme avec un appel HTTP) sur tous les
// attributs de cette "company" sauf sur l'attribut "users".

// Partie 4 : Créer une fonction prenant en paramètre un "id" de "company" et un
// nouvel "user" dont le nom est "Delgado", le prénom "Juan", ayant 35 ans et
// une voiture. Le nouvel "user" doit être ajouté à la liste des "users" de cette
// "company" et avoir un "id" généré automatiquement. La fonction doit aussi modifier
// l'attribut "usersLength" de "company".

// Partie 5 : Créer une fonction prenant en paramètre un "id" de "company" et
// permettant de faire un PUT (comme avec un appel HTTP) sur cette "company" sauf
// sur l'attribut "users".

// Partie 6 : Créer une fonction prenant en paramètre un "id" de "company" et un
// "id" de "user". La fonction doit supprimer cet "user" de la liste des "users"
// de la "company" et modifier l'attribut "usersLength" de "company".

// Partie 7 : Créer une fonction prenant en paramètre un "id" de "company" et un
// "id" de "user" permettant de faire un PATCH (comme avec un appel HTTP) sur cet
// "user".

// Partie 8 : Créer une fonction prenant en paramètre un "id" de "company" et un
// "id" de "user" permettant de faire un PUT (comme avec un appel HTTP) sur cet
// "user".

// Partie 9 : Créer une fonction prenant en paramètre deux "id" de "company" et
// un "id" de "user". La fonction doit permettre de transférer l'user en paramètre
// de la 1re "company" à la 2e "company". L'attribut "usersLength" de chaque
// "company" doit être mis à jour.
