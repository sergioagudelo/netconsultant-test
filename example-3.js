import {cleanConsole, createAll} from './data';
import {fixInformation} from './example-1';
const companies = createAll();

cleanConsole(3, companies);

const validateUpperCase = (companies) => {
  // I checked only first letter of company name, firstName and lastName, since example 1
  // only return the first letter as upper case
  if (!companies || !Array.isArray(companies) || !companies.length) {
    return {};
  }
  const upperCase = (text) => {
    const regexp = /^[A-Z]/;
    if (regexp.test(text) || text === '') {
      return true;
    } else {
      return false;
    }
  };
  for (const company of companies) {
    if (upperCase(company.name)) {
      if (!!company && !!company.users && !!company.users.length) {
        for (const user of company.users) {
          if ((!upperCase(user.firstName) || !upperCase(user.lastName))) {
            return false;
          }
        };
      }
    } else {
      return false;
    }
  };
  return true;
};

console.log('---- EXAMPLE 3 --- ', validateUpperCase(fixInformation(companies)));

// -----------------------------------------------------------------------------
// INSTRUCCIONES EN ESPAÑOL

// Cree una función tomando la variable "companies" como parámetro y devolviendo
// un booleano que valida que todos los nombres de las empresas y los atributos
// "firstName" y "lastName" de "users" están en mayúsculas.
// Debes probar la operación de esta función importando la función creada
// en el "example-1.js".

// -----------------------------------------------------------------------------
// INSTRUCTIONS IN ENGLISH

// Create a function taking the "companies" variable as a parameter and returning
// a boolean validating that all the names of the companies and the attributes "firstName"
// and "lastName" of "users" are capitalized. You must test the operation
// of this function by importing the function created for "example-1.js".

// -----------------------------------------------------------------------------
// INSTRUCTIONS EN FRANÇAIS

// Créer une fonction prenant en paramètre la variable "companies" et renvoyant
// un booléen validant que tous les noms des "company" et les attributs "firstName"
// et "lastName" des "users" sont en majuscules. Vous devez tester le fonctionnement
// de cette fonction en important la fonction créée pour "example-1.js".
