import {cleanConsole, createAll} from './data';
const companies = createAll();

cleanConsole(5, companies);

const createObjectFive = (companies) => {
  if (!companies || !Array.isArray(companies) || !companies.length) {
    return {};
  }

  const tableUsers = [];
  let ages = [];
  const object = {};

  companies.map( (company) => {
    if (!!company && !!company.users && !!company.users.length) {
      company.users.map((user) => {
        user.company = company.name;
        tableUsers.push(user);
        ages.push(user.age);
      });
    }
  });

  object.size = tableUsers.length;

  object.average = ages.reduce( (accumulatorAge, currentAge) => {
    return accumulatorAge + currentAge;
  })/object.size;

  ages = [];
  object.hasCar = tableUsers.filter( (user) => {
    if (user.car) {
      ages.push(user.age);
      return user;
    }
  }).length;

  object.averageWithCar = ages.reduce( (accumulatorAge, currentAge) => {
    return accumulatorAge + currentAge;
  })/ages.length;

  return object;
};

console.log('---- EXAMPLE 5 --- ', createObjectFive(companies));

// -----------------------------------------------------------------------------
// INSTRUCCIONES EN ESPAÑOL

// Use la función creada en el ejemplo 4 para crear una nueva función tomando
// como parámetro la variable "companies" y devuelve un nuevo objeto con los
// siguientes atributos:
//     'size' => total de "users"
//     'average' => edad promedio de "users"
//     'hasCar' => total de "users" propietarios de un carro
//     'averageWithCar' => edad promedio de los "users" con un carro

// -----------------------------------------------------------------------------
// INSTRUCTIONS IN ENGLISH

// Use the function created in example 4 to create a
// new function taking as parameter the "companies" variable and returning
// a new object with the following attributes:
//     'size' => number of "users"
//     'average' => average age of "users"
//     'hasCar' => number of "users" owning a car
//     'averageWithCar' => average age of users with a car

// -----------------------------------------------------------------------------
// INSTRUCTIONS EN FRANÇAIS

// Utiliser la fonction créée dans l'exemple 4 pour créer une
// nouvelle fonction prenant en paramètre la variable "companies" et renvoyant
// un nouvel objet avec les attributs suivants :
//     'size' => nombre de "users"
//     'average' => moyenne d'âge des "users"
//     'hasCar' => nombre de "users" possédant une voiture
//     'averageWithCar' => moyenne d'âge des "users" possédant une voiture
